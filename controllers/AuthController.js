const io = require('../io');
const requestJson = require("request-json");
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumon6ed/collections/"; //url base de la bbdd
const mLabApiKey = "apiKey=oOlV1cnAn2-TsomRj6xIOp5fEaVe5uDo";

function loginV1 (req,res){

  console.log("POST /apitechu/v1/login");

  console.log(req.body)
  var login = req.body.login;
  var pass = req.body.password;
  var loginResult = {};

  if (!login || !pass){
    loginResult.message="Email y passoword son necesarios";
    res.send(loginResult);
  }

  var users = require('../usuarios.json');
  for (user of users){
    if (user.email == login && user.password== pass){
      console.log("Login OK");
      loginResult.message="Login Ok";
      loginResult.userId=user.id;
      user.logged=true;
      io.writeUserDataToFile(users, '../usuarios.json');
      break;
    }else{
      console.log("ERROR de login");
      loginResult.message="Error de login";
    }
  }
  res.send(loginResult);
}

function logoutV1 (req,res){

  console.log("POST /apitechu/v1/logout");
  var users = require('../usuarios.json');
  var id = req.body.id;
  var logoutResult = {};

  for (user of users){
    if (user.id == id && user.logged==true){
      console.log("Logout OK");
      logoutResult.message="Logout Ok";
      logoutResult.userId=user.id;
      delete user.logged;
      io.writeUserDataToFile(users, '../usuarios.json');
      break;
    }else{
      console.log("ERROR de logout");
      logoutResult.message="Error de logout";
    }
  }
  res.send(logoutResult);
}

function loginV2 (req,res){

  console.log("POST /apitechu/v2/login");

  console.log(req.body)
  var login = req.body.login;
  var pass = req.body.password;

  if (!login || !pass){
    var response={"message":"Email y passoword son necesarios"}
    res.send(response);
  }
  //cliente http a la url base de Mlab definida
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = 'q={"email":"' + login + '"}';
  //petición
  httpClient.get("user?"+query +'&'+mLabApiKey,
    function (err, resMLab, body){
      console.log("user?"+query +'&'+mLabApiKey);
      if (err){
        var response={"message":"Error de login"}
        res.status(500);
        res.send(response);
      }

      //hacer la comprobación de la password
      console.log("Get usuarios mlab sin error");

      if(body.length==0 || !crypt.checkPassword(pass,body[0].password)){
        var response={"message":"Error de login"}
        res.status(404);
        res.send(response);
      }

      console.log(body[0]);
      console.log("Checking password OK");
      //actualización del usuario en Mlab
      var putBody ='{"$set":{"logged":true}}';
      console.log(putBody);
      httpClient.put("user?"+query +'&'+mLabApiKey,JSON.parse(putBody),
        function (errPut, resMLabPut, bodyPut){
          console.log("Actualizando el usuario");
          if (!errPut){
            console.log("Usuario logado");
            var response={
              "userId": body[0].id,
              "message":"Login Ok"
            }
            res.send(response);
          }else {
            var response={"message":"Error de login"}
            res.status(500);
            res.send(response);
          }
        }
      );
    }
  );
}

function logoutV2 (req,res){

  console.log("POST /apitechu/v2/logout/:id");
  var id = req.params.id;

  //cliente http a la url base de Mlab definida
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = 'q={"id":' + id + '}';
  //petición
  httpClient.get("user?"+query +'&'+mLabApiKey,
    function (err, resMLab, body){
      console.log("user?"+query +'&'+mLabApiKey);
      if (!err){
        console.log("Get usuarios mlab sin error");
        if (body.length>0){
          if(body[0].logged=true){
            console.log("Usuario estaba logado");

            //actualización del usuario en Mlab
            var putBody ='{"$unset":{"logged":""}}';
            console.log(putBody);
            httpClient.put("user?"+query +'&'+mLabApiKey,JSON.parse(putBody),
                function (errPut, resMLabPut, bodyPut){
                  console.log("Actualizando el usuario");
                  if (!errPut){
                    console.log("Usuario deslogado");
                    var logout={};
                    logout.userId=body[0].id;
                    logout.message="Logout Ok";
                    res.send(logout);
                  }else {
                    var logout={};
                    logout.message="Error de logout";
                    res.status(500);
                    res.send(logout);
                  }
                }
            );
          }else {
            var logout={};
            logout.message="Error de logout";
            res.status(404);
            res.send(logout);
          }
        }else{
          var logout={};
          logout.message="Error de logout";
          res.status(404);
          res.send(logout);
        }
      } else {
        var logout={};
        logout.message="Error de logout";
        res.status(500);
        res.send(logout);
    }
  }
  );
}


module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
