const io = require('../io');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumon6ed/collections/"; //url base de la bbdd
const mLabApiKey = "apiKey=oOlV1cnAn2-TsomRj6xIOp5fEaVe5uDo";
const requestJson = require("request-json");

function getUsersV1 (req,res){
  console.log("GET /apitechu/v1/users");

  //se devuelve el fichero, se busca a partir de __dirname que es la ruta actual
  //res.sendFile('usuarios.json', {root: __dirname});

  var users = require('../usuarios.json');
  var result = new Object();
  result.users=users;

  if (req.query.$count=="true"){
    console.log("Hay que devolver numero de usuarios");
    result.total = users.length;
  }
  if (req.query.$top){
    result.users=users.slice(0,req.query.$top);
  }
  res.send(result);

}

function createUserV1 (req,res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body);
  console.log ("first_name: " + req.body.first_name);
  console.log ("last_name: " + req.body.last_name);
  console.log ("email: " + req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);

  io.writeUserDataToFile(users, '../usuarios.json');

  console.log ("Usuario añadido con éxito");

  res.send({"msg": "Usuario añadido con éxito"});

}

function deleteUserV1 (req,res){
  console.log("DELETE /apitechu/v1/users");

  var users = require('../usuarios.json');

  users.forEach(function (user,index) {
    if(user.id==req.params.id){
      console.log("USUARIO: " +JSON.stringify(user));
      users.splice(index,1);
    }
  });

  io.writeUserDataToFile(users, './usuarios.json');

  res.send({"msg": "Usuario Borrado"});
  console.log("Usuario con id " + req.params.id + " borrado");
}



function getUsersV2 (req,res){
  console.log("GET /apitechu/v2/users");
  //console.log(req);

  //cliente http a la url base de Mlab definida
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  //petición
  httpClient.get("user?"+mLabApiKey,
    function (err, resMLab, body){
      console.log(err);
      var response = !err ? body : {"msg": "Error obteniendo usuarios"}
      res.send(response);
    }
  );
}

function createUserV2(req,res){
    console.log("POST /apitechu/v2/users");
    console.log ("first_name: " + req.body.first_name);
    console.log ("last_name: " + req.body.last_name);
    console.log ("email: " + req.body.email);

    var newUser = {
      "id": req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password": crypt.hash(req.body.password)
    }
    //cliente http a la url base de Mlab definida
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    //petición
    httpClient.post("user?"+mLabApiKey, newUser,
      function (err, resMLab, body){
        console.log("Usuario añadido con éxito");
        res.send({"msg": "Usuario añadido con éxito"});
      }
    );

}


function getUserByIdV2 (req,res){
  console.log("GET /apitechu/v2/users/:id");
  console.log('getUserByIdV2');
  console.log(req.params);

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  //cliente http a la url base de Mlab definida
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  //petición
  httpClient.get("user?"+query + "&" +mLabApiKey,
    function (err, resMLab, body){
      if (err){
        console.log("ERROR MLAB " + err);
        var response = {"msg": "Error obteniendo usuarios"}
        res.status (500);
        res.send(response);
      }else {
        if (body.length>0){
          console.log("Hay body! ");
          var response = body[0];
          res.send(response);
        }else {
          var response = {"msg": "Error obteniendo usuarios"}
          res.status(404);
          res.send(response);
        }
      }
    }
  );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
