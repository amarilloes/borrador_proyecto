const mocha = require('mocha');//framework test
const chai = require('chai');//aserciones adicionales a las que tiene por defecto mocha
const chaihttp = require('chai-http');//añadido a chai que nos permite hacer peticiones http

chai.use(chaihttp);

//BDD: behaviour driven development
var should = chai.should();
var server = require('../server');//incluimos el arranque de server para que los test sean autocontenidos.

//definición de u na test suite
describe('First unit test',
  function(){
    it('Test that duckcuckgo works',
      function(done){
        chai.request('http://www.duckduckgo.com')
        //chai.request('https://developer.mozilla.org/en-US/djdjjdjdj')
        .get('/')
        .end(//a realizar cuando finalice la petición
          function(err,res){
            console.log("Request has finished");

            //ASERCIONES
            res.should.have.status(200);

            done();//indicar cuando termina la ejecución. Se indica al framework cuando empezar a comprobar aserciones
          }//funciontionend

        )//end
      }//done

    )//it
  }
)//describe

/* TEST APICTECHU*/

describe('Test de API usuarios',
  function(){
    it('Prueba de que API usuarios responde',
      function(done){
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err,res){
            console.log("Request has finished");
            res.should.have.status(200);
            done();
          }//funciontionend

        )//end
      }//done

    ), //it

    it('Prueba de que API devuelve lista de usuarios correcta',
      function(done){
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err,res){
            console.log("Request has finished");
            res.should.have.status(200);
            res.body.users.should.be.a("array");
            for (user of res.body.users){
              user.should.have.property('email');
              user.should.have.property('password');
            }

            done();
          }//funciontionend

        )//end
      }//done

    )//it lista correcta
  }
)//describe
