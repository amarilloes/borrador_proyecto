
const fs = require('fs');

function writeUserDataToFile(data, file){

  var jsonData = JSON.stringify(data);

  fs.writeFile(file, jsonData, "utf8",function(err){
    if (err){
      console.log(err);
    }else {
      console.log("Usuario persistido");
    }
  }

);

}

module.exports.writeUserDataToFile = writeUserDataToFile;
