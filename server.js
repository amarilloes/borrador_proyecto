const express = require('express');
const app = express(); //inicialización del famework express
const port = process.env.PORT || 3000;
const bodyParser= require('body-parser');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');

app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

//añadimos una ruta, se disponibilidad el get para la ruta y se define funcion manejadora
app.get('/apitechu/v1/hello',
  function (req,res){
    console.log("GET /apitechu/v1/hello");

    //respuesta del api, por defecto HTML
    res.send({"msg":"Hola desde imagen DEFINITIVA"});
  }
);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/logout', authController.logoutV1);
app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

//FUNCION TEST DE envio de información a servidor

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);
    console.log("Query string");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);
    console.log("Body");
    console.log(req.body);
    console.log(req.body.first_name);

  }
);
